import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:faker/faker.dart';

import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var faker = new Faker();

  String tanggal = "2020-08-01";

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      ListView.builder(
        itemCount: 7,
        itemBuilder: (context, index) => ListTile(
          leading: CircleAvatar(
            backgroundImage:
                NetworkImage("https://picsum.photos/id/${237 + index}/200/300"),
            backgroundColor: Colors.grey,
          ),
          title: Text(faker.person.name()),
          // subtitle: Text(faker.internet.email()),
          subtitle: Text(
            "${DateFormat.yMMMd().format(DateTime.now())}",
            // "${DateTime.now()}"
          ),
        ),
      ),
      Center(child: Text("Page 2")),
      Center(child: Text("Page 3")),
      Center(child: Text("Page 4")),
      Center(child: Text("Page 5")),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text("Latihan Pakage"),
      ),
      body: widgets[currentIndex],
      bottomNavigationBar: ConvexAppBar(
          items: [
            TabItem(icon: Icons.home, title: 'Home'),
            TabItem(icon: Icons.map, title: 'Discovery'),
            TabItem(icon: Icons.add, title: 'Add'),
            TabItem(icon: Icons.message, title: 'Message'),
            TabItem(icon: Icons.people, title: 'Profile'),
          ],
          backgroundColor: Colors.teal,
          initialActiveIndex: currentIndex,
          onTap: (int i) => setState(() {
                currentIndex = i;
              })),
    );
  }
}
