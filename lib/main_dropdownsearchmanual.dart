import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dropdown Search"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: DropdownSearch<String>(
          popupProps: PopupProps.menu(
            showSelectedItems: true,
            disabledItemFn: (String s) => s.startsWith('I'),
          ),
          items: [
            "Brazil",
            "Italia",
            "Tunisia",
            'Canada',
            "Irak",
            "Inggris",
            "Yaman"
          ],
          dropdownDecoratorProps: DropDownDecoratorProps(
            dropdownSearchDecoration: InputDecoration(
              labelText: "Country",
              hintText: "country in menu mode",
            ),
          ),
          onChanged: (value) => print(value),
          selectedItem: "Brazil",
        ),
      ),
    );
  }
}
