import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:http/http.dart' as http;
import 'package:latihan_package/models/city.dart';

import 'models/province.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  String? idProv;
  final String apiKey =
      "daef91368146d3c25589ca080f15019a8a35a5a564f646c20a948ff95149d342";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Dropdown Search API")),
      body: ListView(
        padding: EdgeInsets.all(18),
        children: [
          DropdownSearch<Province>(
            selectedItem: Province(name: "Pilih Provinsi", id: ""),
            asyncItems: (text) async {
              var response = await http.get(Uri.parse(
                  "https://api.binderbyte.com/wilayah/provinsi?api_key=$apiKey"));
              if (response.statusCode != 200) {
                return [];
              }
              List allProvince =
                  (json.decode(response.body) as Map<String, dynamic>)["value"];
              List<Province> allModelProvince = [];

              allProvince.forEach(
                (element) {
                  allModelProvince
                      .add(Province(id: element["id"], name: element["name"]));
                },
              );
              return allModelProvince;
            },
            itemAsString: (item) => item.name,
            // onChanged: (value) => print(value?.toJson()),
            onChanged: (value) => idProv = value?.id,
          ),
          SizedBox(height: 15),
          // if (idProv != null)
          DropdownSearch<City>(
            asyncItems: (text) async {
              print(idProv);
              var response = await http.get(Uri.parse(
                  "https://api.binderbyte.com/wilayah/kabupaten?api_key=$apiKey&id_provinsi=$idProv"));
              if (response.statusCode != 200) {
                return [];
              }

              List allCity =
                  (json.decode(response.body) as Map<String, dynamic>)["value"];
              List<City> allModelCity = [];

              allCity.forEach(
                (element) {
                  allModelCity.add(
                    City(
                      id: element["id"],
                      idProvinsi: element["id_provinsi"],
                      name: element["name"],
                    ),
                  );
                },
              );
              return allModelCity;
            },

            itemAsString: (item) => item.name,
            onChanged: (value) => print(value?.toJson()),
            // onChanged: (value) => print(value?.name),
          ),
        ],
      ),
    );
  }
}
